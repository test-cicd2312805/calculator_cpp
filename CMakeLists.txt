cmake_minimum_required(VERSION 3.10)
project(CalculatorProject)

add_executable(CalculatorProject main.cpp Calculator.cpp Calculator.h)
