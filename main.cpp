#include "calculator.h"
#include <iostream>

int main() {
    calculator calculator;

    // Test addition
    int sum = calculator.add(5, 3);
    std::cout << "5 + 3 = " << sum << std::endl;

    // Test subtraction
    int difference = calculator.subtract(8, 3);
    std::cout << "8 - 3 = " << difference << std::endl;

    return 0;
}
